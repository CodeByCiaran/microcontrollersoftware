#include <Stepper.h>
#include <AS5047P.h>

// Set up stepper motors
#define STEPS         400
#define STEERING_CE   4
#define LOADER_CE     25
#define GRABBER_CE    33

// Ultrasonic Sensor pins
#define echoPin 18 
#define trigPin 17

#define AS5047P_CHIP_SELECT_PORT 37 
// define the spi bus speed 
#define AS5047P_CUSTOM_SPI_BUS_SPEED 100000

// initialize a new AS5047P sensor object.
AS5047P as5047p(AS5047P_CHIP_SELECT_PORT, AS5047P_CUSTOM_SPI_BUS_SPEED);

// create instances of the stepper class
Stepper steering(STEPS, 0, 1, 2, 3);
Stepper loader(STEPS, 9, 8, 7, 6);
Stepper grabber(STEPS, 14, 15, 29, 28);

const int baleTouchPin = 26;
int baleTouch = 0;

// the previous reading from the analog input
int previous = 150;
unsigned long previousTime = 0;
unsigned long jetsonSteerTime = 0;

long duration; // duration of sound wave travel
int distance, distanceToSend;  // ultrasonic sensor distance

char outputArr[6][20];

int medianBuffer[30], sortedBuffer[30];
int iteration = 0;

bool closed = false;
unsigned long closeStartTime, closeStopTime = 0;
bool started = false;

unsigned long upStartTime, upStopTime = 0;
bool upStarted = false;

String turnStr, grabberUpStr, grabberDownStr, openStr, closeStr, autoStr;
int turnDbl=100, grabberUpInt, grabberDownInt, openInt, closeInt, autoInt;
int autoSteer, jetsonSteer = 100;

int autoBuffer[100];
int autoBufferSize = 100;
int autoCount = 0;
int sum = 0;
double autoIntAvg;


void setup() {
  // initialize both serial ports:
  Serial.begin(9600);
  Serial5.begin(9600);
  Serial8.begin(9600);

  pinMode(baleTouchPin, INPUT);
  
  // set the speed of the motors (RPM)
  steering.setSpeed(60);
  loader.setSpeed(30);
  grabber.setSpeed(60);

  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

  pinMode(STEERING_CE, OUTPUT);
  digitalWrite(STEERING_CE, HIGH);

  pinMode(LOADER_CE, OUTPUT);
  digitalWrite(LOADER_CE, HIGH);

  pinMode(GRABBER_CE, OUTPUT);
  digitalWrite(GRABBER_CE, HIGH);

  // Setup ultrasonic pins
  pinMode(trigPin, OUTPUT); 
  pinMode(echoPin, INPUT);

  while (!as5047p.initSPI()) {
    Serial.println(F("Can't connect to the AS5047P sensor! Please check the connection..."));
    // Flash LED if not connected
    digitalWrite(13, LOW);
    delay(100);
    digitalWrite(13, HIGH);
    delay(100);
  }
}

void loop() {
  unsigned long currentTime = millis();
  
  // read the angle value from the AS5047P sensor
  int angle = as5047p.readAngleDegree();  

  baleTouch = digitalRead(baleTouchPin);

  if (autoCount<autoBufferSize){
    autoBuffer[autoCount] = autoInt;
    autoCount++;
  }
  else {
    sum = 0;
    for (int i=0; i<autoBufferSize; i++){
      sum = sum + autoBuffer[i];
    }
    autoIntAvg = sum/autoBufferSize;
    for (int i=0; i<autoBufferSize-1; i++){
      //shift everything left
      autoBuffer[i] = autoBuffer[i+1];
    }
    //add latest value to end of buffer
    autoBuffer[autoBufferSize-1] = autoInt;
  }

  //Read Steering data from Jetson Nano
  if (Serial8.available()) {
    //update jetsonSteer value every 100 ms
    if(currentTime>jetsonSteerTime+100){
      autoSteer = Serial8.parseInt();
      if((autoSteer != 100) && (autoSteer <= 200) && (autoSteer >=0)){
        jetsonSteer = autoSteer;
        jetsonSteerTime = currentTime;
      }
      }
    }  
  else{
      //Wait a while and then decide that no bale is present so reset steer
      if(currentTime>jetsonSteerTime + 1000){
        jetsonSteer = 200;
      }
  }


  

  //Decode incoming serial data from Motor Controller 
  if((currentTime - previousTime) >= 200){
    // read from serial port
    if (Serial5.available()) {
    uint8_t arrNum=0, index=0;
    while(1){
        char incomingByte = Serial5.read();
    
        if(incomingByte == ','){
          outputArr[arrNum][index] = "\0";
          arrNum++;
          index = 0;
        }
        else if (incomingByte == '@'){
          arrNum = 0;
          index = 0;
          
          //Clear Buffer
          while(Serial5.available()){  
            int stupidExcess = Serial5.read();
          }
          break;
        }
        else {
          outputArr[arrNum][index] = incomingByte;
          index=index+1;
        }
      }
      
     
     //Split incoming array into individual arrays
     turnStr = String(outputArr[0]);
     grabberUpStr = String(outputArr[1]);
     grabberDownStr = String(outputArr[2]);
     openStr = String(outputArr[3]);
     closeStr = String(outputArr[4]);
     autoStr = String(outputArr[5]);
  
     //Convert to numbers
     turnDbl = turnStr.toInt();
     grabberUpInt = grabberUpStr.toInt();
     grabberDownInt = grabberDownStr.toInt();
     openInt = openStr.toInt();
     closeInt = closeStr.toInt();
     autoInt = autoStr.toInt();
    }
    previousTime = currentTime;
  }

    Serial.print("\nTurn: ");
    Serial.print(turnDbl);
    Serial.print("\tPrevious");
    Serial.print(previous);

  
    /**
     * Steering
     * Read angle from sensor
     * Read angle from app
     * Turn motor until app angle == sensor angle
     * step appAngle-sensorAngle
     */

  //if in autonomous mode, overwrite app value with camera value
  if(autoIntAvg > 0.5){
    //set precision of steering
    //jetsonSteer = map(jetsonSteer, 50, 150, 0, 200);
    if(jetsonSteer>=50 && jetsonSteer<=150){
      turnDbl=(jetsonSteer-50)*200/100;
    }
    else if(jetsonSteer<50){
      turnDbl = 1;
    }
    else if (jetsonSteer>150){
      turnDbl = 200;
    }
  }
  
  angle = map(angle, 0, 360, 0, 200);
  //angle = 100;
  if(turnDbl>angle+2) {
    //turn right
    steering.step(1);
  }
  else if (turnDbl<angle-2 && turnDbl!=0){
    //turn left
    steering.step(-1);
  }


  // Loader
  if (grabberUpInt == 1){
    loader.step(1);
  }
  else if (grabberDownInt == 1) {
    loader.step(-1);
  }

  // Grabber
  if (openInt == 1){
    grabber.step(-1);
    closed = false;
  }
  else if (closeInt == 1) {
    grabber.step(1);
  }

/*
// Ultrasonic Sensor code
  // Clear the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Set the trigPin HIGH for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Read the echoPin
  duration = pulseIn(echoPin, HIGH);
  // Calculate the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  // Median moving average filter
  if (iteration > 29){
    for(int fillBufferCount=0; fillBufferCount<30; fillBufferCount++){
      sortedBuffer[fillBufferCount] = medianBuffer[fillBufferCount];
    }
    
    // Sort buffer
    for(int i=0;i<30;i++){
        for(int j=i+1;j<30;j++){
            if(sortedBuffer[i] > sortedBuffer[j]){
               int a = sortedBuffer[i];
               sortedBuffer[i] = sortedBuffer[j];
               sortedBuffer[j] = a;
            }
        }
    }
    // Shift all elements of buffer one to the left
    for(int c=0; c<29; c++){
      medianBuffer[c] = medianBuffer[c+1];
    } 
    medianBuffer[29] = distance;
  }
  else {
    // Fill up buffer
    medianBuffer[iteration] = distance;
    iteration++;
  }
  
  
  distanceToSend = sortedBuffer[15];
  
  // Send distance to motor controller
  Serial5.print(distance);
  Serial5.println("@");
*/


  if (baleTouch == HIGH /*&& !closed*/ && autoIntAvg>0.5 && (closeStopTime-closeStartTime<18000)){
    if(!started){
      closeStartTime = currentTime;
      started = true;
    }
    //Close grabber
    grabber.step(10);
    closeStopTime = currentTime;
  }
  else if (baleTouch == HIGH /*&& !closed*/ && autoIntAvg>0.5 && (closeStopTime-closeStartTime>18000) && upStopTime-upStartTime<30000){
    if(!upStarted){
      upStartTime = currentTime;
      upStarted = true;
    }
    //Close grabber
    loader.step(10);
    upStopTime = currentTime;
  }
  
  else {
    closed = false;
  }
  
}
