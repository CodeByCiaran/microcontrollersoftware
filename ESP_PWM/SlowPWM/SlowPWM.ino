#define ENABLE_A            2
#define ENABLE_B            0

#define pMOS_B_PIN            14
#define nMOS_B_PIN            27

#define nMOS_A_PIN            12


void setup() {
  pinMode(ENABLE_B, OUTPUT);
  digitalWrite(ENABLE_B, HIGH);

  pinMode(ENABLE_A, OUTPUT);
  digitalWrite(ENABLE_A, HIGH);

  pinMode(nMOS_B_PIN, OUTPUT);
  digitalWrite(nMOS_B_PIN, LOW);
  
  pinMode(nMOS_A_PIN, OUTPUT);
  digitalWrite(nMOS_A_PIN, HIGH);

  pinMode(pMOS_B_PIN, OUTPUT);

}

void loop() {
  digitalWrite(pMOS_B_PIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1);                       // wait for a second
  digitalWrite(pMOS_B_PIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1);

}
