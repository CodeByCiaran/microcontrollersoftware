#define LEDC_CHANNEL_0     0
#define LEDC_CHANNEL_1     2
#define LEDC_CHANNEL_2     4
#define LEDC_CHANNEL_3     6

// use 13 bit precission for LEDC timer
#define LEDC_TIMER_13_BIT  13

// use 5000 Hz as a LEDC base frequency
#define LEDC_BASE_FREQ     5000
#define LEDC_BASE_FREQ2    3000
#define LEDC_BASE_FREQ3    2000
#define LEDC_BASE_FREQ4    1000

// fade LED PIN (replace with LED_BUILTIN constant for built-in LED)
#define LED_PIN            13
#define LED_PIN2           12
#define LED_PIN3           26//14
#define LED_PIN4           27

#define ENABLE_A              2
#define ENABLE_B              0

int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// Arduino like analogWrite
// value has to be between 0 and valueMax
void ledcAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 8191 from 2 ^ 13 - 1
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}

void setup() {

  //Chip enable for A MOSFET driver
  pinMode(ENABLE_A, OUTPUT);
  digitalWrite(ENABLE_A, HIGH);
  
  //Chip enable for B MOSFET driver
  pinMode(ENABLE_B, OUTPUT);
  digitalWrite(ENABLE_B, HIGH);
    
  // Setup timer and attach timer to a led pin
  ledcSetup(LEDC_CHANNEL_0, LEDC_BASE_FREQ, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_PIN, LEDC_CHANNEL_0);

  ledcSetup(LEDC_CHANNEL_1, LEDC_BASE_FREQ2, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_PIN2, LEDC_CHANNEL_1);

  ledcSetup(LEDC_CHANNEL_2, LEDC_BASE_FREQ3, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_PIN3, LEDC_CHANNEL_2);

  ledcSetup(LEDC_CHANNEL_3, LEDC_BASE_FREQ4, LEDC_TIMER_13_BIT);
  ledcAttachPin(LED_PIN4, LEDC_CHANNEL_3);

}

void loop() {
  // set the brightness on LEDC channel 0
  ledcAnalogWrite(LEDC_CHANNEL_0, brightness);

  ledcAnalogWrite(LEDC_CHANNEL_1, brightness);

  ledcAnalogWrite(LEDC_CHANNEL_2, brightness);

  ledcAnalogWrite(LEDC_CHANNEL_3, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
