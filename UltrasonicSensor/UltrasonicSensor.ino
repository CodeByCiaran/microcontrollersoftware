
#define echoPin 18 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 17 //attach pin D3 Arduino to pin Trig of HC-SR04

// defines variables
long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
int iteration = 0;
int distanceToSend;
int medianBuffer[30], sortedBuffer[30];
int bufferLength = 100;

unsigned long previousTime = 0;

void setup() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
  Serial5.begin(9600); // // Serial Communication is starting with 9600 of baudrate speed
}
void loop() {

  unsigned long currentTime = millis();
  
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)
  
  /*
  // Median moving average filter
  if (iteration > bufferLength-1){
    for(int fillBufferCount=0; fillBufferCount<bufferLength; fillBufferCount++){
      sortedBuffer[fillBufferCount] = medianBuffer[fillBufferCount];
    }
    
    // Sort buffer
    for(int i=0;i<bufferLength;i++){
        for(int j=i+1;j<bufferLength;j++){
            if(sortedBuffer[i] > sortedBuffer[j]){
               int a = sortedBuffer[i];
               sortedBuffer[i] = sortedBuffer[j];
               sortedBuffer[j] = a;
            }
        }
    }
    // Shift all elements of buffer one to the left
    for(int c=0; c<bufferLength-1; c++){
      medianBuffer[c] = medianBuffer[c+1];
    } 
    medianBuffer[bufferLength-1] = distance;
  }
  else {
    // Fill up buffer
    medianBuffer[iteration] = distance;
    iteration++;
  }

  distanceToSend = sortedBuffer[bufferLength/2];
*/
  
  currentTime = currentTime + 1;
 
  
    if(distance!=0&&(currentTime - previousTime) >= 200){
      Serial5.print(distance);
      Serial5.print("@");
      previousTime = currentTime;
    }
}
