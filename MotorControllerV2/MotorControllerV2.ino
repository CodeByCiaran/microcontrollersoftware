#define PWM_CHANNEL_A     0
#define PWM_CHANNEL_B     2

#define PWM_CHANNEL_BP    4

// use 13 bit precission for LEDC timer
#define PWM_TIMER_13_BIT  13

// Set PWM Frequency
#define PWM_FREQ     10000

// Pins
#define pMOS_L_PIN            14
#define pMOS_R_PIN            13
#define nMOS_L_PIN            12
#define nMOS_R_PIN            27

#define ENABLE_A              2
#define ENABLE_B              0

//######## Bluetooth stuff ########
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

unsigned long previousTime = 0;
unsigned long previousTime2 = 0;

BluetoothSerial SerialBT;
char outputArr[20][7];
String forwardStr, turnStr, grabberUpStr, grabberDownStr, openStr, closeStr, autoStr;
double forwardDbl, turnDbl;
int grabberUpInt, grabberDownInt, openInt, closeInt, autoInt;
//################

String ultrasonicStr;
int ultrasonicInt;
char outputArr2[10];

int dutyCycle = 0;    
double fadeAmount = 0.001;    

bool forward = true;
double backwardsDbl;

// Arduino like analogWrite
// value has to be between 0 and valueMax
void PWMAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 8191 from 2 ^ 13 - 1
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}

void setup() {

  //Serial comms to Teensy
  Serial.begin(9600);

  SerialBT.begin("Bale Collector"); //Bluetooth device name
  
  //Chip enable for A MOSFET driver
  pinMode(ENABLE_A, OUTPUT);
  digitalWrite(ENABLE_A, HIGH);
  
  //Chip enable for B MOSFET driver
  pinMode(ENABLE_B, OUTPUT);
  digitalWrite(ENABLE_B, HIGH);

  // Set nMOS pins as outputs
  pinMode(nMOS_L_PIN, OUTPUT);
  pinMode(nMOS_R_PIN, OUTPUT);

  // Set pMOS pins as outputs
  pinMode(pMOS_L_PIN, OUTPUT);
  pinMode(pMOS_R_PIN, OUTPUT);
  
  // Setup timer and attach timer to a pwm pins
  // Side A
  ledcSetup(PWM_CHANNEL_A, PWM_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(nMOS_L_PIN, PWM_CHANNEL_A);

  //Side B
  ledcSetup(PWM_CHANNEL_B, PWM_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(nMOS_R_PIN, PWM_CHANNEL_B);

  //Accelerator
  adcAttachPin(38);
  analogSetClockDiv(255); // 1338mS
}

void loop() {
  //While Bluetooth connection is available
  if (SerialBT.available()) {
   
   char stringArr[100];

    //Decode incoming bluetooth data
   for(int i=1, j=0, arrNum=0; i<=100; i++){
    char incomingByte = SerialBT.read();
  
    if(incomingByte==','){
      arrNum++;
      j=0;
    }
    else if(incomingByte=='@'){
      //Empty buffer
      SerialBT.flush();
      break;
    }
    else {
      outputArr[arrNum][j] = incomingByte;
      j++;
    }
   } 

   
   //Split incoming array into individual arrays
   forwardStr = String(outputArr[0]);
   turnStr = String(outputArr[1]);
   grabberUpStr = String(outputArr[2]);
   grabberDownStr = String(outputArr[3]);
   openStr = String(outputArr[4]);
   closeStr = String(outputArr[5]);
   autoStr = String(outputArr[6]);

   //Convert to numbers
   forwardDbl = forwardStr.toDouble();
   turnDbl = turnStr.toDouble();
   grabberUpInt = grabberUpStr.toInt();
   grabberDownInt = grabberDownStr.toInt();
   openInt = openStr.toInt();
   closeInt = closeStr.toInt();
   autoInt = autoStr.toInt();
  }

  
  if(forwardDbl >= 0){
    if(forward){
      driveForward(forwardDbl);
    //goNowhere();
    //driveBackwards(forwardDbl);
    }
    else delay(2);  //give MOSFETs time to turn off
    forward = true;
    backwardsDbl=0;
  }
  else if (forwardDbl < 0){
    if(!forward){
      //goNowhere();
      backwardsDbl = forwardDbl*(-1);
      driveBackwards(backwardsDbl);
      //forwardDbl = forwardDbl*(-1);
      //driveBackwards(forwardDbl);

    }
    else delay(2);  // give MOSFETs time to turn off
    forward = false;
  }

  // Get rid of negative value from -1 to 0
  int turnVal = 0;
  turnVal = (turnDbl + 1)*100;

  unsigned long currentTime = millis();
  if((currentTime - previousTime) >= 100){
     //Serial.write(serialSend);
     
      //Send turn data to control system
      Serial.print(turnVal);
      Serial.print(",");
      Serial.print(grabberUpInt);
      Serial.print(",");
      Serial.print(grabberDownInt);
      Serial.print(",");
      Serial.print(openInt);
      Serial.print(",");
      Serial.print(closeInt);
      Serial.print(",");
      Serial.print(autoInt);
      Serial.print("@");

     previousTime = currentTime;
  }

/*
  //Read data from Control System
  unsigned long currentTime2 = millis();
  if((currentTime - previousTime) >= 200){
    if (Serial.available()) {
     
     char stringArr[100];
  
      //Decode incoming bluetooth data
     for(int i=1, j=0; i<=100; i++){
      char incomingByte = Serial.read();
    
      if(incomingByte=='@'){
        outputArr2[j] = '\0';
        //Clear Buffer
          while(Serial.available()){  
            int stupidExcess = Serial.read();
          }
          j=0;
        break;
      }
      else {
        outputArr2[j] = incomingByte;
        j++;
      }
     } 
  
     
     //Split incoming array into individual arrays
     ultrasonicStr = String(outputArr2);
  
     //Convert to numbers
     ultrasonicInt = ultrasonicStr.toInt();
     //Serial.print("\nThrottle: ");
     //Serial.print(ultrasonicInt);
    }
    previousTime2 = currentTime2;
  }

  */
}

void goNowhere(){
  //Turn off pMOS R
  digitalWrite(pMOS_R_PIN, HIGH);
  //Turn off nMOS L
  digitalWrite(nMOS_L_PIN, LOW);
  
  // Turn off pMOS L
  digitalWrite(pMOS_L_PIN, HIGH);
  // Turn off nMOS R
  digitalWrite(nMOS_R_PIN, LOW);

}

void driveForward(double speed){
  /*
   * Turn on pMOS R
   * Switch nMOS L
   */
  PWMAnalogWrite(PWM_CHANNEL_B, 0);
  //Turn off pMOS L
  digitalWrite(pMOS_L_PIN, HIGH);
  //Turn off nMOS R
  digitalWrite(nMOS_R_PIN, LOW);
  
  // Turn on pMOS R
  digitalWrite(pMOS_R_PIN, LOW);
  
  // Switch nMOS L
  speed = speed*255;
  dutyCycle = (int) speed;
  PWMAnalogWrite(PWM_CHANNEL_A, dutyCycle);
}

void driveBackwards(double speed){
  /*
   * Turn on pMOS L
   * Switch nMOS R
   */
  PWMAnalogWrite(PWM_CHANNEL_A, 0);
  //Turn off pMOS R
  digitalWrite(pMOS_R_PIN, HIGH);
  //Turn off nMOS L
  digitalWrite(nMOS_L_PIN, LOW);
  
  // Turn on pMOS L
  digitalWrite(pMOS_L_PIN, LOW);
  
  // Switch nMOS R
  speed = speed*255;
  dutyCycle = (int) speed;
  PWMAnalogWrite(PWM_CHANNEL_B, dutyCycle);
}
