//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Evandro Copercini - 2018
//
//This example creates a bridge between Serial and Classical Bluetooth (SPP)
//and also demonstrate that SerialBT have the same functionalities of a normal Serial

#include "BluetoothSerial.h"
#include <CSV_Parser.h>


#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;

float forward=0, turn=0;
int armUp=0, armDown=0, openGrabber=0, closeGrabber=0;

void setup() {
  Serial.begin(115200);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() {
  /*if (Serial.available()) {
    SerialBT.write(Serial.read());
  }*/
  if (SerialBT.available()) {
   char * incomingData = (char*)SerialBT.read();
   CSV_Parser cp(incomingData, /*format*/ "ffdddd", /*has_header*/ false);
   float   *float1 = (float*)cp[0];
   float   *float2 = (float*)cp[1];
   int16_t *int1 = (int16_t*)cp[2];
   int16_t *int2 = (int16_t*)cp[3];
   int16_t *int3 = (int16_t*)cp[4];
   int16_t *int4 = (int16_t*)cp[5];

   Serial.print("forward: ");
   Serial.print(*float1);
/*
   forward = float1;
   turn = float2;
   armUp = int1;
   armDown = int2;
   openGrabber = int3;
   closeGrabber = int4;
*/
   for(int row = 0; row < cp.getRowsCount(); row++) {
    Serial.print(row, DEC);
    Serial.print(". Forward = ");
    Serial.println(float1[row]);
    Serial.println(row, DEC);
    Serial.print(". Turn = ");
    Serial.println(float2[row], DEC);
    
  }
  }
  delay(20);
}
