#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

BluetoothSerial SerialBT;

char outputArr[20][7];
String forwardStr, turnStr, grabberUpStr, grabberDownStr, openStr, closeStr, autoStr;
double forwardDbl, turnDbl;
int grabberUpInt, grabberDownInt, openInt, closeInt, autoInt;

void setup() {
  Serial.begin(9600);
  SerialBT.begin("ESP32test"); //Bluetooth device name
  //Serial.println("The device started, now you can pair it with bluetooth!");
}

void loop() {


  if (SerialBT.available()) {

   char stringArr[100];

   for(int i=1, j=0, arrNum=0; i<=100; i++){
    char incomingByte = SerialBT.read();
  
    if(incomingByte==','){
      arrNum++;
      j=0;
    }
    else if(incomingByte=='@'){
      //Empty buffer
      SerialBT.flush();
      break;
    }
    else {
      outputArr[arrNum][j] = incomingByte;
      j++;
    }
   } 

  forwardStr = String(outputArr[0]);
  turnStr = String(outputArr[1]);
  grabberUpStr = String(outputArr[2]);
  grabberDownStr = String(outputArr[3]);
  openStr = String(outputArr[4]);
  closeStr = String(outputArr[5]);
  autoStr = String(outputArr[6]);

  forwardDbl = forwardStr.toDouble();
  turnDbl = turnStr.toDouble();
  grabberUpInt = grabberUpStr.toInt();
  grabberDownInt = grabberDownStr.toInt();
  openInt = openStr.toInt();
  closeInt = closeStr.toInt();
  autoInt = autoStr.toInt();

  // Get rid of negative value from -1 to 0
  turnDbl = turnDbl + 1;
  turnDbl = turnDbl*100;
   
  Serial.print("\nForward: ");
  Serial.print(forwardDbl);
  Serial.print(" Turn: ");
  Serial.print(turnDbl);
  Serial.print(" Up: ");
  Serial.print(grabberUpInt);
  Serial.print(" Down: ");
  Serial.print(grabberDownInt);
  Serial.print(" Open: ");
  Serial.print(openInt);
  Serial.print(" Close: ");
  Serial.print(closeInt);
  Serial.print(" Autonomous: ");
  Serial.print(autoInt);
  }
  //delay(20);
}
