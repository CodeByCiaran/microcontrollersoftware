#define PWM_CHANNEL_A     0
#define PWM_CHANNEL_B     2

#define PWM_CHANNEL_BP    4

// use 13 bit precission for LEDC timer
#define PWM_TIMER_13_BIT  13

// Set PWM Frequency
#define PWM_FREQ     10000

// Pins
#define pMOS_A_PIN            13
#define pMOS_B_PIN            14
#define nMOS_A_PIN            12
#define nMOS_B_PIN            27

#define ENABLE_A              2
#define ENABLE_B              0

double brightness = 0;    // how bright the LED is
double fadeAmount = 0.001;    // how many points to fade the LED by

// Arduino like analogWrite
// value has to be between 0 and valueMax
void PWMAnalogWrite(uint8_t channel, uint32_t value, uint32_t valueMax = 255) {
  // calculate duty, 8191 from 2 ^ 13 - 1
  uint32_t duty = (8191 / valueMax) * min(value, valueMax);

  // write duty to LEDC
  ledcWrite(channel, duty);
}

void setup() {
  //Chip enable for A MOSFET driver
  pinMode(ENABLE_A, OUTPUT);
  digitalWrite(ENABLE_A, HIGH);
  
  //Chip enable for B MOSFET driver
  pinMode(ENABLE_B, OUTPUT);
  digitalWrite(ENABLE_B, HIGH);

  // Set nMOS pins as outputs
  //pinMode(nMOS_A_PIN, OUTPUT);
  pinMode(nMOS_B_PIN, OUTPUT);

  // Set pMOS pins as outputs
  pinMode(pMOS_A_PIN, OUTPUT);
  pinMode(pMOS_B_PIN, OUTPUT);
  
  // Setup timer and attach timer to a pwm pins
  // Side A
  ledcSetup(PWM_CHANNEL_A, PWM_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(nMOS_A_PIN, PWM_CHANNEL_A);

  //Side B
  ledcSetup(PWM_CHANNEL_B, PWM_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(nMOS_B_PIN, PWM_CHANNEL_B);

  //Side BP
  ledcSetup(PWM_CHANNEL_BP, PWM_FREQ, PWM_TIMER_13_BIT);
  ledcAttachPin(pMOS_B_PIN, PWM_CHANNEL_BP);

  //Accelerator
  adcAttachPin(38);
  analogSetClockDiv(255); // 1338mS
}

void loop() {
  driveBackwards();
  //PdriveForward();
  //testnMOSB();
}

void driveForward(){
  /*
   * Turn on pMOS A
   * Switch nMOS B
   */
  //Turn off pMOS B
  digitalWrite(pMOS_B_PIN, HIGH);
  //Turn off nMOS A
  digitalWrite(nMOS_A_PIN, LOW);
  
  // Turn on pMOS A
  digitalWrite(pMOS_A_PIN, LOW);

    // Switch nMOS B
  while(1){  
    PWMAnalogWrite(PWM_CHANNEL_B, brightness);
  
    // change the brightness for next time through the loop:
    brightness = brightness + fadeAmount;
  
    // reverse the direction of the fading at the ends of the fade:
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
  }
}

void PdriveForward(){
  /*
   * Turn on nMOS A
   * Switch pMOS B
   */
  //Turn off nMOS B
  digitalWrite(nMOS_B_PIN, LOW);
  //pMOS A pulled high with hardware. Set this pin to Low to avoid overlap protection
  digitalWrite(pMOS_A_PIN, LOW);
  
  // Turn on nMOS A
  digitalWrite(nMOS_A_PIN, HIGH);

  //brightness = 127;
  

    // Switch pMOS B
  while(1){  
    brightness = analogRead(38);
    brightness = map(brightness, 0, 4096, 0, 255);
    PWMAnalogWrite(PWM_CHANNEL_BP, brightness);
  /*
    // change the brightness for next time through the loop:
    brightness = brightness + fadeAmount;
  
    // reverse the direction of the fading at the ends of the fade:
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
    */
  }
}

void goNowhere(){
  while(1){
    //Turn off pMOS B
    digitalWrite(pMOS_B_PIN, HIGH);
    //Turn off nMOS A
    digitalWrite(nMOS_A_PIN, LOW);
    
    // Turn off pMOS A
    digitalWrite(pMOS_A_PIN, HIGH);
    // Turn off nMOS B
    digitalWrite(nMOS_B_PIN, LOW);
  }
}


void driveBackwards(){
  /*
   * Turn on pMOS B
   * Switch nMOS A
   */
  //Turn off pMOS A
  // Pulled High with hardware so keep low to overcome overlap protection
  digitalWrite(pMOS_A_PIN, LOW);
  //Turn off nMOS B
  digitalWrite(nMOS_B_PIN, LOW);
  
  // Turn on pMOS B
  digitalWrite(pMOS_B_PIN, LOW);

  //brightness = 50;
  
    // Switch nMOS A
  while(1){  
    brightness = analogRead(38);
    brightness = map(brightness, 0, 4096, 0, 255);
    PWMAnalogWrite(PWM_CHANNEL_A, brightness);
  /*
    // change the brightness for next time through the loop:
    brightness = brightness + fadeAmount;
  
    // reverse the direction of the fading at the ends of the fade:
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
  */
  }
}


/*
 * Test Functions
 */
void testnMOSB(){
  /*
   * Turn off pMOS A
   * Turn off nMOS A
   * Turn off pMOS B
   * Switch nMOS B
   */
  //Turn on pMOS B
  digitalWrite(pMOS_B_PIN, LOW);
  //Turn off nMOS A
  digitalWrite(nMOS_A_PIN, LOW);
  
  // Turn off pMOS A
  digitalWrite(pMOS_A_PIN, HIGH);

    // Switch nMOS B
  while(1){  
    PWMAnalogWrite(PWM_CHANNEL_B, brightness);
  
    // change the brightness for next time through the loop:
    brightness = brightness + fadeAmount;
  
    // reverse the direction of the fading at the ends of the fade:
    if (brightness <= 0 || brightness >= 255) {
      fadeAmount = -fadeAmount;
    }
  }
}
