#include <Stepper.h>
#include <AS5047P.h>

// Set up stepper motors
#define STEPS         400
#define STEERING_CE   4
#define LOADER_CE     25
#define GRABBER_CE    33

#define AS5047P_CHIP_SELECT_PORT 37 
// define the spi bus speed 
#define AS5047P_CUSTOM_SPI_BUS_SPEED 100000

// initialize a new AS5047P sensor object.
AS5047P as5047p(AS5047P_CHIP_SELECT_PORT, AS5047P_CUSTOM_SPI_BUS_SPEED);

// create instances of the stepper class
Stepper steering(STEPS, 0, 1, 2, 3);
Stepper loader(STEPS, 9, 8, 7, 6);
Stepper grabber(STEPS, 14, 15, 29, 28);

// the previous reading from the analog input
int previous = 150;
unsigned long previousTime = 0;

char outputArr[6][20];

String turnStr, grabberUpStr, grabberDownStr, openStr, closeStr;
int turnDbl, grabberUpInt, grabberDownInt, openInt, closeInt;

void setup() {
  // initialize both serial ports:
  Serial.begin(9600);
  Serial5.begin(9600);
  
  // set the speed of the motor to 30 RPMs
  steering.setSpeed(60);
  loader.setSpeed(15);
  grabber.setSpeed(60);

  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

  pinMode(STEERING_CE, OUTPUT);
  digitalWrite(STEERING_CE, HIGH);

  pinMode(LOADER_CE, OUTPUT);
  digitalWrite(LOADER_CE, HIGH);

  pinMode(GRABBER_CE, OUTPUT);
  digitalWrite(GRABBER_CE, HIGH);

  while (!as5047p.initSPI()) {
    Serial.println(F("Can't connect to the AS5047P sensor! Please check the connection..."));
    // Flash LED if not connected
    digitalWrite(13, LOW);
    delay(100);
    digitalWrite(13, HIGH);
    delay(100);
  }
}

void loop() {
  // read the angle value from the AS5047P sensor
  int angle = as5047p.readAngleDegree();     

  //Decode incoming serial data from Motor Controller 
  unsigned long currentTime = millis();
  if((currentTime - previousTime) >= 200){
    // read from serial port
    if (Serial5.available()) {
    uint8_t arrNum=0, index=0;
    while(1){
        char incomingByte = Serial5.read();
    
        if(incomingByte == ','){
          outputArr[arrNum][index] = "\0";
          arrNum++;
          index = 0;
        }
        else if (incomingByte == '@'){
          arrNum = 0;
          index = 0;
          
          //Clear Buffer
          while(Serial5.available()){  
            int stupidExcess = Serial5.read();
          }
          break;
        }
        else {
          outputArr[arrNum][index] = incomingByte;
          index=index+1;
        }
      }
      
     
     //Split incoming array into individual arrays
     turnStr = String(outputArr[0]);
     grabberUpStr = String(outputArr[1]);
     grabberDownStr = String(outputArr[2]);
     openStr = String(outputArr[3]);
     closeStr = String(outputArr[4]);
  
     //Convert to numbers
     turnDbl = turnStr.toInt();
     grabberUpInt = grabberUpStr.toInt();
     grabberDownInt = grabberDownStr.toInt();
     openInt = openStr.toInt();
     closeInt = closeStr.toInt();
    }
    previousTime = currentTime;
  }

    Serial.print("\nTurn: ");
    Serial.print(turnDbl);
    Serial.print("\tPrevious");
    Serial.print(previous);

  
    /**
     * Steering
     * Read angle from sensor
     * Read angle from app
     * Turn motor until app angle == sensor angle
     * step appAngle-sensorAngle
     */
  angle = map(angle, 0, 360, 0, 200);
  if(turnDbl>angle+2) {
    //turn one way
    steering.step(1);
  }
  else if (turnDbl<angle-2){
    //turn other way
    steering.step(-1);
  }


  // Loader
  if (grabberUpInt == 1){
    loader.step(1);
  }
  else if (grabberDownInt == 1) {
    loader.step(-1);
  }

  // Grabber
  if (openInt == 1){
    grabber.step(-1);
  }
  else if (closeInt == 1) {
    grabber.step(1);
  }
}
