import serial
import time


def blink():
	print("Starting blink....")
	while True:
		#bluetooth.write(b"On"+str.encode(str(i)))
		bluetooth.write(b"Up")
		print("Up")
		time.sleep(1)
		#bluetooth.write(b"Off"+str.encode(str(i)))
		bluetooth.write(b"Down")
		print("Down")
		time.sleep(1)



print("Start")
port = "COM19"
bluetooth = serial.Serial(port, 115200)	#Start communications with the bluetooth unit
print("Connected")
bluetooth.flushInput() 				#This gives the bluetooth a little kick
blink()


bluetooth.close() 					#Otherwise the connection will remain open until a timeout which ties up the /dev/thingamabob
#print("Done")

